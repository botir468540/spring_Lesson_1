package com.example.spring_1lesson.entity;

import javax.persistence.*;

//Company malumotlarini service, controller yordamida ResposeEntity
//        qaytaradigan to'liq REST full API yozing. Bunda Address(street, homeNumber)
//        Company(corpName, directorName, Address) Department(name, Company)
//        Worker(name, phoneNumber, Address, Department) malumotlari bo'lsin.
//        Proyektni git ga yuklab, javob sifatida linkni yuboring.


@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String street;

    @Column
    private String homeNumber;


    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }
}
