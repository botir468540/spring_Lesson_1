package com.example.spring_1lesson.controller;


import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/address")
public class AddressController {


    private final AddressService service;

    public AddressController(AddressService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllAddress() {
        return ResponseEntity.ok(service.getAllAddresses());

    }

    @PostMapping
    public ResponseEntity<?> addAddress(@RequestBody Address address) {
        Address newAddress = this.service.addAddress(address);
        return ResponseEntity.ok(newAddress);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getOneAddress(@PathVariable(name = "id") Integer id) {
        return ResponseEntity.ok().body(service.getOne(id));
    }

    @PutMapping
    public ResponseEntity<?> updateAddress(@RequestBody Address address){
        return ResponseEntity.ok().body(service.editOne(address));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name="id")Integer id ){
        return ResponseEntity.ok().body(service.deleteOne(id));
    }


}
