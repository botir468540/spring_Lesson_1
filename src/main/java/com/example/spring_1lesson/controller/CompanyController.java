package com.example.spring_1lesson.controller;


import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    private final CompanyService service;

    public CompanyController(CompanyService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllCompany(){
        return ResponseEntity.ok().body(service.getAllCompany());
    }

    @PostMapping
    public ResponseEntity<?> addCompany(@RequestBody Company company){
        return ResponseEntity.ok().body(service.addCompany(company));
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getOneCompany(@PathVariable(name = "id") Integer id){
        return ResponseEntity.ok().body(service.getOne(id));
    }

    @PutMapping
    public ResponseEntity<?> updateCompany(@RequestBody Company company){
        return ResponseEntity.ok().body(service.editOne(company));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name ="id") Integer id){
        return ResponseEntity.ok().body(service.deleteOne(id));
    }

}
