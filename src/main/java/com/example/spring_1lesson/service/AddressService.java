package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.repository.AddressRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService implements AddressServiceInterface {


    private final AddressRepository repository;

    public AddressService(AddressRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Address> getAllAddresses() {
        List<Address> allAddresses = repository.findAll();
        return allAddresses;
    }

    @Override
    public Address getOne(Integer id) {
        Optional<Address> byId = repository.findById(id);

        if (byId.isPresent()) {
            Address address = byId.get();
            return address;
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "" +
                    "Missing the required parameter ID");
        }
    }

    @Override
    public Address addAddress(Address address) {
        return this.repository.save(address);
    }

    @Override
    public Address editOne(Address address) {
        Optional<Address> byId = Optional.empty();
        if (address.getId() != null) {
            byId = repository.findById(address.getId());
        }

        Address address1 = byId.get();
        address1.setStreet(address.getStreet());
        address1.setHomeNumber(address.getHomeNumber());
        Address save=repository.save(address1);


        return save;
    }

    @Override
    public Address deleteOne(Integer id) {
        Optional<Address> byId = repository.findById(id);

        if(byId.isPresent()){
            repository.deleteById(id);
        }else{
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "" +
                    "Missing the required parameter ID");
        }

        return null;
    }
}
