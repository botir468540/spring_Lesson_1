package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.entity.Company;

import java.util.List;

public interface CompanyInterface {

    public List<Company> getAllCompany();

    public Company getOne(Integer id);

    public Company addCompany(Company address);

    public Company editOne(Company address);

    public Company deleteOne(Integer id);
}
