package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;

import java.util.List;

public interface AddressServiceInterface {

    public List<Address> getAllAddresses();

    public Address getOne(Integer id);

    public Address addAddress(Address address);

    public Address editOne(Address address);

    public Address deleteOne(Integer id);

}
