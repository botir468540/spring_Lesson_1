package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.repository.AddressRepository;
import com.example.spring_1lesson.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;


@Service
public class CompanyService implements CompanyInterface{

    private final CompanyRepository repository;

    public CompanyService(CompanyRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Company> getAllCompany() {
        List<Company> allAddresses = repository.findAll();
        return allAddresses;
    }

    @Override
    public Company getOne(Integer id) {
        Optional<Company> byId = repository.findById(id);

        if (byId.isPresent()) {
            Company company = byId.get();
            return company;
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "" +
                    "Missing the required parameter ID");
        }
    }

    @Override
    public Company addCompany(Company address) {
        return this.repository.save(address);
    }

    @Override
    public Company editOne(Company address) {
        Optional<Company> byId = Optional.empty();
        if (address.getId() != null) {
            byId = repository.findById(address.getId());
        }

        Company address1 = byId.get();
        address1.setDirectorName(address.getDirectorName());
        address1.setCorpName(address.getCorpName());
        address1.setAddress(address.getAddress());
        Company save=repository.save(address1);


        return save;
    }

    @Override
    public Company deleteOne(Integer id) {
        Optional<Company> byId = repository.findById(id);

        if(byId.isPresent()){
            repository.deleteById(id);
        }else{
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "" +
                    "Missing the required parameter ID");
        }

        return null;
    }
}
